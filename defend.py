"""
Simple program to show moving a sprite with the keyboard.

This program uses the Arcade library found at http://arcade.academy

Artwork from https://kenney.nl/assets/space-shooter-redux
"""

import math
import os
import random

import arcade


SPRITE_SCALING = 0.5

# Set the size of the screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Variables controlling the player
PLAYER_LIVES = 3
PLAYER_SPEED_X = 0
PLAYER_START_X = SCREEN_WIDTH / 2
PLAYER_START_Y = SCREEN_HEIGHT / 2
PLAYER_SHOT_SPEED = 4
PLAYER_AMMO_DEFAULT = 20

# Size of spaces in virtual grid (Playfield)
# This is the distance travelled by hyperspace
GRID_SIZE = 60
 
# How likely is a PowerUp to spawn (Instead of an Enemy)
POWER_UP_SPAWN_PERCENTAGE = 0.2

EXPLOSION_LIFE_TIME = 0.3

# Enemy spawn points
# Up, right, down , left
ENEMY_SPAWN_POINTS = [
    (SCREEN_WIDTH/2, SCREEN_HEIGHT),
    (SCREEN_WIDTH, SCREEN_HEIGHT/2),
    (SCREEN_WIDTH/2, 0),
    (0, SCREEN_HEIGHT/2)
    ]

# Time between spawning enemies in seconds
ENEMY_SPAWN_SPEED = 1.0

# Look for textures in this dir
TEXTURES_DIR = "images/textures"

class PowerUp(arcade.Sprite):
    """
    A power Up to be picked up by the Player
    """
    
    # Types of power ups
    types = [
        {'name': 'Lives +10', 'image': 'pill_green.png', 'mod_player_lives': 10},
        {'name': 'Score +50', 'image': 'pill_blue.png', 'mod_player_score': 50},
        {'name': 'Ammo +10', 'image': 'pill_red.png', 'mod_player_ammo': 10},
        {'name': 'Hyperspace +1', 'image': 'pill_yellow.png', 'mod_player_hyperspace': 1},
        ]

    def __init__(self, target): 
        """
        Setup new PowerUp object
        target: A sprite to point/move towards
        """

        # What type of PowerUp am I?
        self.type = random.choice(PowerUp.types)

        # Set the graphics to use for the sprite
        super().__init__("images/" + self.type['image'], SPRITE_SCALING)

        # Decide on where to spawn
        my_spawn_point = random.choice(ENEMY_SPAWN_POINTS)

        # Move to spawn point
        self.center_x, self.center_y = my_spawn_point

        # Point towards player
        dX = target.center_x - self.center_x
        dY = target.center_y - self.center_y

        # Angle in radians
        angle_r = math.atan2(dY, dX)

        # Calculate x and y speeds based on angle
        self.change_x = math.cos(angle_r)
        self.change_y = math.sin(angle_r)

        # Starts with random angle
        self.angle = random.randint(0, 360)
        # Speed of rotation
        self.rot_speed = random.random() * 2

    def update(self):
        """
        Move the sprite
        """
        # Update position
        self.center_x += self.change_x
        self.center_y += self.change_y
        self.angle += self.rot_speed


class Enemy(arcade.Sprite):

    def __init__(self, target):
        """
        Setup new Enemy object
        target: A sprite to point/move towards
        """

        # Set the graphics to use for the sprite
        super().__init__("images/playerShip1_red.png", SPRITE_SCALING)
        
        # Decide on where to spawn
        my_spawn_point = random.choice(ENEMY_SPAWN_POINTS)
        
        # Move to spawn point
        self.center_x, self.center_y = my_spawn_point
        
        # Value to player when enemy killed
        self.value = 100
        
        # Point towards player
        dX = target.center_x - self.center_x 
        dY = target.center_y - self.center_y
        
        # Angle in radians
        angle_r = math.atan2(dY, dX)
        
        # Calculate x and y speeds based on angle
        self.change_x = math.cos(angle_r)
        self.change_y = math.sin(angle_r)

        # Fix angle because sprite is pointing in wrong direction
        # I should really fix the graphics
        self.angle = math.degrees(angle_r) 
        self.angle -= 90
        
    def update(self):
        """
        Move the sprite
        """
        # Update position
        self.center_x += self.change_x
        self.center_y += self.change_y

class Explosion(arcade.AnimatedTimeSprite):
    """
    An explosion. Textures shared across all objects of the class.
    Textures: https://kenney.nl/assets/smoke-particles
    """

    # List of explosion textures shared by the class
    textures = []
    
    # Path to the dir with explosion textures
    dir_path = os.path.join(TEXTURES_DIR, 'Explosion')
    
    # Load textures (Sorted alphabetically)
    for file_name in sorted(os.listdir(dir_path)):
        # The path to the image file
        file_path = os.path.join(dir_path, file_name)
        
        # Load the image in to the list of textures
        textures.append(arcade.load_texture(file_path))

    # Exit if no textures were loaded
    if textures == []:
        raise SystemExit("No textures loaded from " + dir_path)


    # Initialize Explosion with default scaling and textures
    def __init__(self, scale = SPRITE_SCALING, **kwargs):

        # Set the scale to use for the sprite
        super().__init__(scale = scale, **kwargs)

        # Use the textures shared by the class
        self.textures = Explosion.textures
        
        # Fixes texture_id errors in some versions? of the library
        super().update_animation()
        
        # Random angle
        self.angle = random.randint(1, 360)
        
        # How long have I existed (Seconds)
        self.age = 0.0

    def update_animation(self, delta_time):
        
        # Call original update_amiation()
        super().update_animation()
        
        # Update age or kill myself!
        if self.age > EXPLOSION_LIFE_TIME:
            self.kill()
        else:
            self.age += delta_time

        # Add own stuff for explosions here
        # FIXME: Lifetime?
        # FIXME: Size?
        

class Player(arcade.Sprite):
    """
    The player
    """

    def __init__(self, **kwargs):
        """
        Setup new Player object
        """
        
        # Graphics to use for Player
        kwargs['filename'] = "images/playerShip1_red.png"
        
        # How much to scale the graphics
        kwargs['scale'] = SPRITE_SCALING

        # Pass arguments to class arcade.Sprite
        super().__init__(**kwargs)

        # How much ammo
        self.ammo = PLAYER_AMMO_DEFAULT

        # How many hyperspace do I have?
        self.hyperspace_left = 0


    def explode(self):
        """
        The player explodes
        """
        # DEMO: Something happens
        self.angle = 45
        
        
    def update(self):
        """
        Move the sprite
        """

        # Update center_x
        self.center_x += self.change_x

        # Don't let the player move off screen
        if self.left < 0:
            self.left = 0
        elif self.right > SCREEN_WIDTH - 1:
            self.right = SCREEN_WIDTH - 1

    def hyperspace(self):
        '''
        Runs when player performs hyperspace
        '''

        # Don't hyperspace if no hyperspace left
        if self.hyperspace_left < 1:
            # No change in spawn points
            return ENEMY_SPAWN_POINTS

        # Player uses a hyperspace
        self.hyperspace_left -= 1

        # New spawn point (x,y) positions
        return [
            (self.center_x, SCREEN_HEIGHT),
            (SCREEN_WIDTH, self.center_y),
            (self.center_x, 0),
            (0, self.center_y)
            ]


class PlayerShot(arcade.Sprite):
    """
    A shot fired by the Player
    """

    def __init__(self, the_shooting_player):
        """
        Setup new PlayerShot object
        """

        # Set the graphics to use for the sprite
        super().__init__("images/laserBlue01.png", SPRITE_SCALING)
 
        self.center_x = the_shooting_player.center_x
        self.center_y = the_shooting_player.center_y
        self.angle = the_shooting_player.angle
        
        # Calculate x and y speeds
        self.change_x = math.cos(math.radians(self.angle + 90)) * PLAYER_SHOT_SPEED
        self.change_y = math.sin(math.radians(self.angle + 90)) * PLAYER_SHOT_SPEED

    def explode(self):
        """
        Player explodes
        """
        pass
        
    def update(self):
        """
        Move the sprite
        """

        # Update position
        self.center_x += self.change_x
        self.center_y += self.change_y

        # FIXME: Shots can leave scren on sides etc. too!
        # Remove shot when over top of screen
        if self.bottom > SCREEN_HEIGHT:
            self.kill()

class MyGame(arcade.Window):
    """
    Main application class.
    """

    def __init__(self, width, height):
        """
        Initializer
        """

        # Call the parent class initializer
        super().__init__(width, height)

        # Variables holding lists of sprites
        self.player_shot_list = None
        self.enemy_list = None
        self.powerup_list = None
        self.explosion_list = None

        # Time (in ms) since last enemy spawned
        self.enemy_spawn_delta_time = None
        
        # Set up the player info
        self.player_sprite = None
        self.player_score = None
        self.player_lives = None

        # Track the current state of what key is pressed
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False

        # Set the background color
        arcade.set_background_color(arcade.color.AMAZON)


    def reset_level(self):
        """ Resets the current level """
        
        # FIXME: Player explodes
        self.player_sprite.explode()
        
        # Remove all enemies
        for e in self.enemy_list:
            e.kill()
        
        # FIXME: Not all enemies are removed above?


    def setup(self):
        """ Set up the game and initialize the variables. """

        # No points when the game starts
        self.player_score = 0

        # Reset enemy spawn time
        self.enemy_spawn_delta_time = 0
                
        # No. of lives
        self.player_lives = PLAYER_LIVES

        # Sprite lists
        self.player_shot_list = arcade.SpriteList()
        self.enemy_list = arcade.SpriteList()
        self.powerup_list = arcade.SpriteList()
        self.explosion_list = arcade.SpriteList()

        # test explosion
        #e = Explosion(
        #    center_x = 100,
        #    center_y = 100,
        #    scale = SPRITE_SCALING/2
        #    )
        #self.explosion_list.append(e)
        
        # Create a Player object
        self.player_sprite = Player(
            center_x=PLAYER_START_X,
            center_y=PLAYER_START_Y
        )

    def on_draw(self):
        """
        Render the screen.
        """

        # This command has to happen before we start drawing
        arcade.start_render()


        # Draw the player shot
        self.player_shot_list.draw()

        # Draw the power ups
        self.powerup_list.draw()
        
        # Draw all Enemy sprites
        self.enemy_list.draw()

        # Draw the player sprite
        self.player_sprite.draw()

        # Draw explosion
        self.explosion_list.draw()

        # Draw player's score on screen
        arcade.draw_text(
            "SCORE: {}".format(self.player_score),  # Text to show
            10,                  # X position
            SCREEN_HEIGHT - 20,  # Y positon
            arcade.color.WHITE   # Color of text
        )
        
        # Draw players life's on screen
        arcade.draw_text(
            "LIVES: {}".format(self.player_lives),  # Text to show
            SCREEN_WIDTH - 80,                  # X position
            SCREEN_HEIGHT - 20,  # Y positon
            arcade.color.WHITE   # Color of text
        )

        # Draw player's ammo on screen
        arcade.draw_text(
            "AMMO: {}".format(self.player_sprite.ammo),  # Text to show
            SCREEN_WIDTH / 2,                  # X position
            SCREEN_HEIGHT - 20,  # Y positon
            arcade.color.WHITE   # Color of text
        )
        
    def on_update(self, delta_time):
        """
        Movement and game logic
        
        delta_time: time since prev call
        """

        # Update time since last enemy spawned
        self.enemy_spawn_delta_time += delta_time
        
        # Spawn an enemy/something
        # FIXME: rename timer variable
        if self.enemy_spawn_delta_time >= ENEMY_SPAWN_SPEED:
            
            # Spawn PowerUp or Enemy
            if random.random() < POWER_UP_SPAWN_PERCENTAGE :
                # Create an enemy object
                new_pu = PowerUp(self.player_sprite)
                
                # Add the enemy to the enemy list
                self.powerup_list.append(new_pu)
            
            else:
                # Create an enemy object
                new_enemy = Enemy(self.player_sprite)
                
                # Add the enemy to the enemy list
                self.enemy_list.append(new_enemy)
                
            # Reset spawn timer
            self.enemy_spawn_delta_time = 0
            
        # Update player sprite
        self.player_sprite.update()

        # Update the player shots
        self.player_shot_list.update()
                
        
        # Update animated exlosions
        for e in self.explosion_list:
            e.update_animation(delta_time)
        
        # POWER UPS #

        # Update the power ups
        self.powerup_list.update()

        # Power ups colliding with player
        powerup_hit_list = arcade.check_for_collision_with_list(
            self.player_sprite,
            self.powerup_list
            )

        # Run through colliding power ups
        for pu in powerup_hit_list:

            # Modify games based on power up modifier
            self.player_lives += pu.type.get('mod_player_lives', 0)
            self.player_sprite.ammo += pu.type.get('mod_player_ammo', 0)
            self.player_sprite.hyperspace_left += pu.type.get('mod_player_hyperspace', 0)
            self.player_score += pu.type.get('mod_player_score', 0)

            # Remove power up
            pu.kill()

        # ENEMIES #

        # Update the enemies
        self.enemy_list.update()

        # A list of Enemies touching the Player
        player_hit_list = arcade.check_for_collision_with_list(
            self.player_sprite,
            self.enemy_list
            )
        
        # Kill all Enemies touching player 
        for e in player_hit_list:
            
            e.kill()
            
            self.player_lives -= 1
            
            
            # Reset level
            if self.player_lives > 0:
                # FIXME: Remove all enemies
                self.reset_level()
            else:
                self.setup()
                # Only a single call to setup
                break
            
            # Kill maximum of 1 Enemy    
            break



        # Look at all enemies
        for enemy in self.enemy_list:
            # Get shots touching current enemy
            hit_list = arcade.check_for_collision_with_list(
                enemy, self.player_shot_list
                )
            
            # Kill enemy, and 1st shot if hit    
            if len(hit_list) > 0:
                # Player scores 
                self.player_score += enemy.value
                
                # Create an Explosion sprite
                e = Explosion(
                    center_x = enemy.center_x,
                    center_y = enemy.center_y,
                    scale = SPRITE_SCALING/3
                    )
                # Add the explosion to list of explosions
                self.explosion_list.append(e)
                
                # Remove Enemy
                enemy.kill()
                
                # Remove first player shot
                hit_list[0].kill()
            
    def on_key_press(self, key, modifiers):
        """
        Called whenever a key is pressed.
        """
        global ENEMY_SPAWN_POINTS

        # Track state of arrow keys
        if key == arcade.key.UP:
            self.up_pressed = True
            self.player_sprite.angle = 0
        elif key == arcade.key.DOWN:
            self.down_pressed = True
            self.player_sprite.angle = 180
        elif key == arcade.key.LEFT:
            self.left_pressed = True
            self.player_sprite.angle = 90
        elif key == arcade.key.RIGHT:
            self.right_pressed = True
            self.player_sprite.angle = -90


        if key == arcade.key.SPACE:
            # FIXME: make this shorter/prettier
            if self.player_sprite.hyperspace_left > 0:
                hyper = True
            else:
                hyper = False

            # Hyperspace if arrow is held
            if self.left_pressed and hyper:
                self.player_sprite.center_x -= GRID_SIZE
                ENEMY_SPAWN_POINTS = self.player_sprite.hyperspace()
            elif self.right_pressed and hyper:
                self.player_sprite.center_x += GRID_SIZE
                ENEMY_SPAWN_POINTS = self.player_sprite.hyperspace()
            elif self.up_pressed and hyper:
                self.player_sprite.center_y += GRID_SIZE
                ENEMY_SPAWN_POINTS = self.player_sprite.hyperspace()
            elif self.down_pressed and hyper:
                self.player_sprite.center_y -= GRID_SIZE
                ENEMY_SPAWN_POINTS = self.player_sprite.hyperspace()
            else:
                # Shoot if no arrow held
                if self.player_sprite.ammo > 0:
                    new_shot = PlayerShot(self.player_sprite)
                    self.player_shot_list.append(new_shot)
                    self.player_sprite.ammo -= 1

    def on_key_release(self, key, modifiers):
        """
        Called whenever a key is released.
        """
        # Hyperspace
        if key == arcade.key.H:
            self.player_sprite.hyperspace = False

        if key == arcade.key.UP:
            self.up_pressed = False
        elif key == arcade.key.DOWN:
            self.down_pressed = False
        elif key == arcade.key.LEFT:
            self.left_pressed = False
        elif key == arcade.key.RIGHT:
            self.right_pressed = False


def main():
    """
    Main method
    """

    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
